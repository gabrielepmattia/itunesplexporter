import xml.etree.ElementTree
import getopt
import sys
from pathlib import Path
import plistlib
import urllib
from time import gmtime, strftime

# e = xml.etree.ElementTree.parse('thefile.xml').getroot()

PLAYLIST_NAME = "Name"
PLAYLIST_TAG = "Playlists"
PLAYLIST_TRACKS = "Playlist Items"
PLAYLIST_AUDIOBOOKS = "Audiobooks"
PLAYLIST_PODCASTS = "Podcasts"
TRACK_ID = "Track ID"
TRACK_LOCATION = "Location"
TRACKS_TAG = "Tracks"

DATE_FORMAT = "%Y-%m-%d %H:%M:%S UTC+00"


def convertUri(input_uri, old_base, new_base):
    # remove "file://"
    new_uri = input_uri[7:]
    if new_base == "":
        return new_uri
    tmp = new_uri.replace(urllib.parse.quote(old_base), urllib.parse.quote(new_base))
    return urllib.parse.unquote(tmp)


def writeM3uPlaylist(uris, path, playlist_name):
    out = "# Generated by itunesplexporter (gitlab.com/gabrielepmattia/itunesplexporter) on {}\n".format(
        strftime(DATE_FORMAT, gmtime()))
    for uri in uris:
        out += "{}\n".format(uri)
    out += "# Written {} tracks for playlist \"{}\"".format(len(uris), playlist_name)

    out_file = open("{}/{}.m3u8".format(path, playlist_name), "w")
    out_file.write(out)
    out_file.close()


def exportPlaylists(library_path, output_path, old_base_uri, new_base_uri):
    library_file = open(library_path, "rb")
    lib = plistlib.load(library_file)
    library_file.close()

    playlists = lib.get(PLAYLIST_TAG)
    tracks = lib.get(TRACKS_TAG)

    for playlist in playlists:
        playlist_tracks = playlist.get(PLAYLIST_TRACKS)
        if playlist_tracks == None or playlist.get(PLAYLIST_AUDIOBOOKS) != None or playlist.get(PLAYLIST_PODCASTS) != None:
            print("=> Skipping %s" % (playlist.get(PLAYLIST_NAME)))
            continue

        print("=> Processing \"%s\" [%d tracks]" % (playlist.get(PLAYLIST_NAME), len(playlist_tracks)), end="")

        uris = []
        skipped = 0
        for track_el in playlist_tracks:
            track_id = track_el[TRACK_ID]
            track = tracks[str(track_id)]

            if track.get(TRACK_LOCATION) == None:
                skipped += 1
                continue

            track_uri = convertUri(track.get(TRACK_LOCATION), old_base_uri, new_base_uri)
            uris.append(track_uri)

        print(" [%d skipped]" % skipped)

        writeM3uPlaylist(uris, output_path, playlist.get(PLAYLIST_NAME))


def main(argv):
    library_path = ""
    output_path = ""
    old_base_dir = ""
    new_base_dir = ""

    usage = "exporter.py"
    try:
        opts, args = getopt.getopt(
            argv, "hk:p:", ["library=", "output-dir=",  "old-base-dir=", "new-base-dir="])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)
    for opt, arg in opts:
        # print(opt + " -> " + arg)
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt in ("-p", "--library"):
            library_path = arg
        elif opt in ("--output-dir"):
            output_path = arg
        elif opt in ("--old-base-dir"):
            old_base_dir = arg
        elif opt in ("--new-base-dir"):
            new_base_dir = arg

    if library_path == "" or output_path == "":
        print("Some needed parameter was not given")
        print(usage)
        sys.exit()

    if not Path(library_path).is_file():
        print("Library does not exist")
        sys.exit()

    print("====== iTunes Exporter ======")
    print("> library_path %s" % library_path)
    print("> output_path %s" % output_path)
    print("> old_base_dir %s" % old_base_dir)
    print("> new_base_dir %s" % new_base_dir)
    print()

    exportPlaylists(library_path, output_path, old_base_dir, new_base_dir)

    print()
    print("> Done!")


if __name__ == "__main__":
    main(sys.argv[1:])
