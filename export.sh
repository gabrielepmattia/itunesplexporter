#!/bin/sh
SCRIPT_DIR=$(dirname $0)
ITUNES_LIBRARY_DIR=/Users/gabrielepmattia/Music/Music
ITUNES_LIBRARY=$ITUNES_LIBRARY_DIR/Library.xml
OLD_BASE_URI=$ITUNES_LIBRARY_DIR/Media
OUTPUT_DIR=$ITUNES_LIBRARY_DIR/Playlists
#OUTPUT_DIR=./tmp

python $SCRIPT_DIR/exporter.py --library "${ITUNES_LIBRARY}" \
                                --output-dir "${OUTPUT_DIR}" \
                                --old-base-dir "${OLD_BASE_URI}" \
                                --new-base-dir "/storage/0000-0000"